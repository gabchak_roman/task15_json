package com.gabchak.view;

import com.gabchak.model.devices.device.Device;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface View {

    void printMessage(String message);

    void print(List<Device> devices);

    void printRedMessage(String message);

}
