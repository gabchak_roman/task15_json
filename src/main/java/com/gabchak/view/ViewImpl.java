package com.gabchak.view;

import com.gabchak.model.devices.device.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ViewImpl implements View {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static Logger log = LogManager.getLogger(ViewImpl.class);

    @Override
    public void printMessage(String message) {
        log.info(message);
    }

    @Override
    public void print(List<Device> devices) {
        if (devices != null) {
            log.info("Devices: ");
            devices.forEach(device -> log.info(device));
        } else {
            log.error("No devices");
        }
    }

    @Override
    public void printRedMessage(String message) {
        log.info(ANSI_RED + message + ANSI_RESET);
    }

}