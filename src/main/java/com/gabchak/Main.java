package com.gabchak;

import com.gabchak.controller.Controller;
import com.gabchak.controller.ControllerImpl;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.ViewImpl;

public class Main {
    public static void main(String[] args) {

        Controller controller = new ControllerImpl(
                new ViewImpl(),
                new ConsoleReader()
        );

        controller.start();

    }
}

