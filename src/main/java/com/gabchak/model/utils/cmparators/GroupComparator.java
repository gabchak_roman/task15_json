package com.gabchak.model.utils.cmparators;

import com.gabchak.model.devices.device.Device;

import java.util.Comparator;

public class GroupComparator implements Comparator<Device> {
    @Override
    public int compare(Device o1, Device o2) {
        return o1.getGroup().compareTo(o2.getGroup());
    }
}
