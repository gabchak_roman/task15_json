package com.gabchak.model.utils.parcers;

import com.gabchak.model.devices.Devices;
import com.gabchak.model.devices.device.Device;
import com.gabchak.model.utils.cmparators.GroupComparator;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GsonParser {

    public List<Device> readJson(String filePath) throws IOException {
        String data = Files.readString(Paths.get(filePath));
        List<Device> list = new Gson().fromJson(data, Devices.class).getDevices();
        list.sort(new GroupComparator());
        return list;
    }

}
