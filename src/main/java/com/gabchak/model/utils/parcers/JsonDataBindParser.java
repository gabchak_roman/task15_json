package com.gabchak.model.utils.parcers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gabchak.model.devices.Devices;
import com.gabchak.model.devices.device.Device;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonDataBindParser {

    public List<Device> parse(String filePath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File(filePath), Devices.class).getDevices();
    }
}
