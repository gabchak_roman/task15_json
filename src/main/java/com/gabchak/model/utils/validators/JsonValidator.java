package com.gabchak.model.utils.validators;

import com.gabchak.view.View;
import com.gabchak.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonValidator {
    private static Logger LOG = LogManager.getLogger(JsonValidator.class);

    public boolean validate(String schemaPath, String dataPath) throws FileNotFoundException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileReader(schemaPath)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileReader(dataPath)));
        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubject);
        } catch (Exception e) {
            LOG.error(e);
            return false;
        }
        return true;
    }

}