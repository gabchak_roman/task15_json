package com.gabchak.model;

import java.io.IOException;

public interface MenuItem {
    String getTitle();

    String getKey();

    void execute() throws IOException;
}
