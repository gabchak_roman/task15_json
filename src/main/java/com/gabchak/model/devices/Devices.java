package com.gabchak.model.devices;

import com.gabchak.model.devices.device.Device;

import java.util.ArrayList;
import java.util.List;

public class Devices {

    private List<Device> devices = new ArrayList<>();

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

}
