package com.gabchak.model.devices.device;

public enum Port {
    COM, USB, LPT, HDMI, VGA, DVI, USB_TYPE_C;
}
