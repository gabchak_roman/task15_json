package com.gabchak.model.devices.device;

public enum DeviceGroup {
    CPU, MOTHERBOARD, RAM, GPU, POWER_BLOCK, KEYBOARD, MONITOR, MOUSE;
}
