package com.gabchak.model.devices.device;

public enum DeviceType {
    CORE, PERIPHERAL;
}
