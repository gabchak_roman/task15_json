package com.gabchak.model.devices.device;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Device {

    private DeviceType type;
    private DeviceGroup group;
    private String name;
    @SerializedName("critical")
    private boolean isCritical;
    @SerializedName("cooler")
    private boolean cooler;
    @SerializedName("energy_consumption")
    private Integer energyConsumption;
    private List<Port> ports;
    private BigDecimal price;
    private String origin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public DeviceGroup getGroup() {
        return group;
    }

    public void setGroup(DeviceGroup group) {
        this.group = group;
    }

    @JsonGetter("critical")
    public boolean isCritical() {
        return isCritical;
    }

    public void setCritical(boolean critical) {
        this.isCritical = critical;
    }

    @JsonGetter("cooler")
    public boolean hasCooler() {
        return cooler;
    }

    public void setCooler(boolean cooler) {
        this.cooler = cooler;
    }

    @JsonGetter("energy_consumption")
    public Integer getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(Integer energy_consumption) {
        this.energyConsumption = energy_consumption;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public List<Port> getPorts() {
        return ports;
    }

    public void addPort(Port port) {
        if (ports == null) {
            ports = new ArrayList<>();
        }
        ports.add(port);
    }

    @Override
    public String toString() {
        return "Device{" +
                "type=" + type +
                ", group=" + group +
                ", name='" + name + '\'' +
                ", isCritical=" + isCritical +
                ", hasCooler=" + cooler +
                ", energyConsumption=" + energyConsumption +
                ", ports=" + ports +
                ", price=" + price +
                ", origin='" + origin + '\'' +
                '}';
    }
}
