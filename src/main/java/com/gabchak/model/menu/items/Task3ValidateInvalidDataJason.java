package com.gabchak.model.menu.items;

import com.gabchak.model.utils.validators.JsonValidator;
import com.gabchak.view.View;

import java.io.FileNotFoundException;

import static java.lang.String.format;

public class Task3ValidateInvalidDataJason extends AbstractMenuItem {

    private static final String NAME = "Validate with Jackson";
    private static final String DETAILS = "'invalid data'";

    public Task3ValidateInvalidDataJason(String key, String title, View view, String data, String schema) {
        super(key, format(TITLE_FORMAT, title, NAME, DETAILS), view, data, schema);
    }

    @Override
    public void execute() throws FileNotFoundException {
        boolean isValid = new JsonValidator()
                .validate(schema, data);
        view.printMessage("Is valid: " + isValid + "      " + DETAILS);
    }
}
