package com.gabchak.model.menu.items;

import com.gabchak.model.devices.device.Device;
import com.gabchak.model.utils.parcers.GsonParser;
import com.gabchak.model.utils.validators.JsonValidator;
import com.gabchak.view.View;

import java.io.IOException;
import java.util.List;

import static java.lang.String.format;

public class Task1ParseByGson extends AbstractMenuItem {
    private static final String NAME = "Parse with GSON";
    private static final String DETAILS = "'sorted by group'";

    public Task1ParseByGson(String key, String title, View view, String data, String schema) {
        super(key, format(TITLE_FORMAT, title, NAME, DETAILS), view, data, schema);
    }

    @Override
    public void execute() throws IOException {
        boolean isValid = new JsonValidator().validate(schema, data);
        if (isValid) {
            view.printRedMessage("Parsed with GSON  " + DETAILS);
            List<Device> devices = new GsonParser().readJson(data);
            view.print(devices);
        }
    }
}
