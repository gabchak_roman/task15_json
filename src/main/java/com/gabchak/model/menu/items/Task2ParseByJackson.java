package com.gabchak.model.menu.items;

import com.gabchak.model.devices.device.Device;
import com.gabchak.model.utils.parcers.JsonDataBindParser;
import com.gabchak.model.utils.validators.JsonValidator;
import com.gabchak.view.View;

import java.io.IOException;
import java.util.List;

import static java.lang.String.format;

public class Task2ParseByJackson extends AbstractMenuItem {
    private static final String NAME = "Parse with Jackson";
    private static final String DETAILS = "'unsorted'";

    public Task2ParseByJackson(String key, String title, View view, String data, String schema) {
        super(key, format(TITLE_FORMAT, title, NAME, DETAILS), view, data, schema);
    }

    @Override
    public void execute() throws IOException {

        boolean isValid = new JsonValidator().validate(schema, data);
        if (isValid) {
            view.printRedMessage("Parsed with Jackson     " + DETAILS);
            List<Device> devices = new JsonDataBindParser().parse(data);
            view.print(devices);
        }
    }
}
