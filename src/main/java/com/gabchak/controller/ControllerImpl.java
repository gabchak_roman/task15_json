package com.gabchak.controller;

import com.gabchak.model.MenuItem;
import com.gabchak.model.menu.items.*;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Map<String, MenuItem> menu;
    private MenuItem notExistingItem;

    private static final Logger LOG = LogManager.getLogger(ControllerImpl.class);

    public ControllerImpl(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
    }

    public void start() {
        buildMenu();
        startMainLoop();
    }

    private void buildMenu() {

        final String data = "src/main/resources/data.JSON";
        final String schema = "src/main/resources/schema.JSON";
        final String invalidData = "src/main/resources/invalid_data.JSON";

        menu = new LinkedHashMap<>();

        List<MenuItem> items = new ArrayList<>();
        items.add(new Task1ParseByGson("1", "Task 1", view, data, schema));
        items.add(new Task2ParseByJackson("2", "Task 2", view, data, schema));
        items.add(new Task3ValidateInvalidDataJason("3", "Task 3", view, invalidData, schema));
        items.add(new Task4ValidateValidDataJackson("4", "Task 4", view, data, schema));
        items.add(new AbstractMenuItem("Q", "Exit", view, null, null) {

            @Override
            public void execute() {
                view.printMessage("Bye");
                System.exit(0);
            }
        });

        notExistingItem = new DummyItem(view, "No such menu item.");

        items.forEach(i -> menu.put(i.getKey(), i));
    }

    private void startMainLoop() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select buildMenu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            cleanConsole();
            try {
                menu.getOrDefault(keyMenu, notExistingItem).execute();
            } catch (Exception e) {
                LOG.error("Can't execute menu item {}.", keyMenu, e);
            }
        } while (true);
    }

    private void outputMenu() {
        System.out.println();
        view.printMessage(" MENU:");
        menu.values().forEach(item -> view.printMessage(
                format(" %s - %s", item.getKey(), item.getTitle())));
    }

    private void cleanConsole() {
        System.out.println("\n".repeat(30));
    }
}